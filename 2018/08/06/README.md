# Generating JSON Web Key Sets

Quick and simple example for generating JSON Web Key Sets using Go.

Tested using

* `go` **1.10**
* `dep` **v0.4.1**

## dep

* `dep ensure -vendor-only`

# Running

```
go run main.go
```

Then visit `http://localhost:8080/jwks.json`, you will get a JSON Web Key set, kind of similar to [Microsoft's](https://login.microsoftonline.com/common/discovery/keys). 
