package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"net/http"
	"os"
	"time"

	jose "gopkg.in/square/go-jose.v2"
)

func main() {
	http.HandleFunc("/jwks.json", func(w http.ResponseWriter, r *http.Request) {
		response, err := json.Marshal(generateJWTWithKeyID())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(os.Stderr, "Error marshaling: %s\n", err)
			return
		}

		if _, err := w.Write(response); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(os.Stderr, "Error writing response: %s\n", err)
			return
		}

		w.Header().Set("Content-Type", "application/json")
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func generateJWTWithKeyID() *jose.JSONWebKeySet {
	rsaKey, _ := rsa.GenerateKey(rand.Reader, 2048)           // XXX Check err
	serialNumber, _ := rand.Int(rand.Reader, big.NewInt(100)) // XXX Check err

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Example Co"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(2 * time.Hour),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, _ := x509.CreateCertificate(rand.Reader, &template, &template, &rsaKey.PublicKey, rsaKey) // XXX Check err
	certificate, _ := x509.ParseCertificate(derBytes)                                                   // XXX Check err

	return &jose.JSONWebKeySet{
		Keys: []jose.JSONWebKey{
			{
				Certificates: []*x509.Certificate{certificate},
				Key:          &rsaKey.PublicKey,
				KeyID:        "someKeyID",
				Use:          "sig",
			},
		},
	}
}
