# Web API implementing the "Authentication flow using OpenID Connect"

Using the [official documentation](https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-protocols-openid-connect-code) as reference.

## Running

Make sure you `dep ensure -vendor-only` before running:

```go
go run main.go -tenant <AZURE-AD-tenant> -appid <AZURE-AD-application-ID> -guids <comma-separated-group-guids>
```

## Using

This program defines 4 endpoints:

* `/login` for our users to log in
* `/logout` for our users to log out
* `/token` used as the _redirect_ URL, returns the computed JWT, it must be _saved_ by used and send it as the `Authentation Beader <token>` value
* `/information` authorized-only resource
