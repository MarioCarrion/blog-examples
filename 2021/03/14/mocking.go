package mocking

import (
	"bytes"
	"encoding/gob"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
)

// ConcreteMemcached uses a real *memcache.Client for testing.
type ConcreteMemcached struct {
	client *memcache.Client
}

// NewConcreteMemcached ...
func NewConcreteMemcached(c *memcache.Client) *ConcreteMemcached {
	return &ConcreteMemcached{
		client: c,
	}
}

// Get ...
func (c *ConcreteMemcached) Get(k string) (string, error) {
	item, err := c.client.Get(k)
	if err != nil {
		return "", err
	}

	res, err := decodeString(item.Value)
	if err != nil {
		return "", err
	}

	return res, nil
}

// Set ...
func (c *ConcreteMemcached) Set(k, v string) error {
	b, err := encodeString(v)
	if err != nil {
		return err
	}

	return c.client.Set(&memcache.Item{
		Key:        k,
		Value:      b,
		Expiration: int32(time.Now().Add(10 * time.Second).Unix()),
	})
}

//go:generate counterfeiter -o mockingtesting/memcache_client.gen.go . MemcacheClient

// MemcacheClient defines the methods required by our Memcached implementation.
type MemcacheClient interface {
	Get(string) (*memcache.Item, error)
	Set(*memcache.Item) error
}

// AdapterMemcached uses a mocked client for testing.
type AdapterMemcached struct {
	client MemcacheClient
}

// NewAdapterMemcached ...
func NewAdapterMemcached(c MemcacheClient) *AdapterMemcached {
	return &AdapterMemcached{
		client: c,
	}
}

// Get ...
func (c *AdapterMemcached) Get(k string) (string, error) {
	item, err := c.client.Get(k)
	if err != nil {
		return "", err
	}

	res, err := decodeString(item.Value)
	if err != nil {
		return "", err
	}

	return res, nil
}

// Set ...
func (c *AdapterMemcached) Set(k, v string) error {
	b, err := encodeString(v)
	if err != nil {
		return err
	}

	return c.client.Set(&memcache.Item{
		Key:        k,
		Value:      b,
		Expiration: int32(time.Now().Add(10 * time.Second).Unix()),
	})
}

func decodeString(v []byte) (string, error) {
	b := bytes.NewReader(v)

	var res string

	if err := gob.NewDecoder(b).Decode(&res); err != nil {
		return "", err
	}

	return res, nil
}

func encodeString(v string) ([]byte, error) {
	var b bytes.Buffer

	if err := gob.NewEncoder(&b).Encode(v); err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}
