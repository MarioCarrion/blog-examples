package counterfeiter_test

import (
	"errors"
	"testing"

	counterfeiter "gitlab.com/MarioCarrion/blog-examples/2019/06/24"
	"gitlab.com/MarioCarrion/blog-examples/2019/06/24/fake"
)

func TestWritesomething(t *testing.T) {
	tests := []struct {
		name     string
		setup    func(*fake.Writer)
		expected bool
	}{
		{
			"true",
			func(*fake.Writer) {},
			true,
		},
		{
			"false: WriteStub",
			func(f *fake.Writer) {
				f.WriteStub = func() error {
					return errors.New("failed stub")
				}
			},
			false,
		},
		{
			"false: WriteReturns",
			func(f *fake.Writer) {
				f.WriteReturns(errors.New("failed mocked"))
			},
			false,
		},
		{
			"false: WriteReturnsOnCall",
			func(f *fake.Writer) {
				f.WriteReturnsOnCall(0, errors.New("returned"))
			},
			false,
		},
	}

	for _, test := range tests {
		tt := test
		writer := fake.Writer{}
		t.Run(tt.name, func(t *testing.T) {
			tt.setup(&writer)

			result := counterfeiter.WriteSomething(&writer)
			if result != tt.expected {
				t.Fatalf("expected %t, got %t", tt.expected, result)
			}
		})
	}
}
