# Server and Clients using Azure Active Directory for Authentication/Authorization

* [`server`](server/) is based on [original the code](https://gitlab.com/MarioCarrion/blog-examples/tree/master/2018/07/16) from my [Azure Active Directory + JWT](https://mariocarrion.com/2018/07/16/azure-active-directory-jwt.html), the main difference is that we indicate if the JWT has no _valid_ GUIDs but it is a valid signed JWT.
* [`client`](client/) is brand new, using the [Server to Server workflow](https://docs.microsoft.com/en-us/azure/active-directory/develop/v1-oauth2-client-creds-grant-flow) using shared secret.
