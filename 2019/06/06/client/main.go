package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

type (
	appConfig struct {
		Tenant              string
		ClientApplicationID string
		ClientSecret        string
		ServerApplicationID string
		ServerURL           string
	}
)

func main() {
	config := appConfig{}

	flag.StringVar(&config.Tenant, "tenant", "", "Azure Tenant")
	flag.StringVar(&config.ClientApplicationID, "cappid", "", "Client Azure Application ID")
	flag.StringVar(&config.ClientSecret, "csecret", "", "Client Azure Application Secret")
	flag.StringVar(&config.ServerApplicationID, "sappid", "", "Server Azure Application ID")
	flag.StringVar(&config.ServerURL, "surl", "", "Server URL")
	flag.Parse()

	if config.Tenant == "" || config.ClientApplicationID == "" || config.ClientSecret == "" ||
		config.ServerApplicationID == "" || config.ServerURL == "" {
		log.Fatal("tenant, cappid, csecret, surl and sappid are required")
	}

	jwt := struct {
		AccessToken string `json:"access_token"`
	}{}

	{
		tokenURL := fmt.Sprintf("https://login.microsoftonline.com/%s/oauth2/token", config.Tenant)

		data := url.Values{}
		data.Set("grant_type", "client_credentials")
		data.Set("client_id", config.ClientApplicationID)
		data.Set("client_secret", config.ClientSecret)
		data.Set("resource", config.ServerApplicationID)
		encoded := data.Encode()

		client := &http.Client{}
		r, _ := http.NewRequest(http.MethodPost, tokenURL, strings.NewReader(encoded))
		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		r.Header.Add("Content-Length", strconv.Itoa(len(encoded)))

		resp, err := client.Do(r)
		if err != nil {
			log.Fatalf("error posting request %s", err)
		}

		defer func() {
			if err = resp.Body.Close(); err != nil {
				fmt.Fprintf(os.Stderr, "there was an error closing body %s\n", err)
			}
		}()
		if err := json.NewDecoder(resp.Body).Decode(&jwt); err != nil {
			log.Fatalf("error decoding response %s", err)
		}
	}

	{
		client := &http.Client{}
		r, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/information", config.ServerURL), nil)
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", jwt.AccessToken))

		resp, err := client.Do(r)
		if err != nil {
			log.Fatalf("error posting request %s", err)
		}
		res, err := ioutil.ReadAll(resp.Body) // XXX
		if err != nil {
			log.Fatalf("error reading all%s", err)
		}

		defer func() {
			if err = resp.Body.Close(); err != nil {
				fmt.Fprintf(os.Stderr, "there was an error closing body %s\n", err)
			}
		}()
		fmt.Println(string(res))
	}
}
