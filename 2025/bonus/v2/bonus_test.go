package bonus_test

import (
	"testing"

	"gitlab.com/MarioCarrion/examples/2025/bonus/v2"
)

func TestCalculator_Percentage(t *testing.T) {
	t.Parallel()

	cal := bonus.NewCalculator(map[string]int{"one": 1, "two": 2})

	if val := cal.Percentage("one"); val != 1 {
		t.Fatalf("got %v, expected: 1", val)
	}

	if val := cal.Percentage("two"); val != 2 {
		t.Fatalf("got %v, expected: 2", val)
	}

	if val := cal.Percentage("unknown"); val != 8 {
		t.Fatalf("got %v, expected: 8", val)
	}
}

func TestDefaultCalculator_Percentage(t *testing.T) {
	t.Parallel()

	cal := bonus.Default()
	if val := cal.Percentage("Mario"); val != 10 {
		t.Fatalf("got %v, expected: 10", val)
	}

	if val := cal.Percentage("Ruby"); val != 15 {
		t.Fatalf("got %v, expected: 15", val)
	}

	if val := cal.Percentage("Other"); val != 8 {
		t.Fatalf("got %v, expected: 8", val)
	}
}
