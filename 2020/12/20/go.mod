module gitlab.com/MarioCarrion/blog-examples/2020/12/20

go 1.15

require (
	github.com/lpar/gzipped/v2 v2.0.2
	github.com/vugu/vgrouter v0.0.0-20200725205318-eeb478c42e5d
	github.com/vugu/vjson v0.0.0-20200505061711-f9cbed27d3d9
	github.com/vugu/vugu v0.3.3
)
