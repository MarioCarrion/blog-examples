package main

import (
	"bytes"
	"encoding/csv"
	"log"

	"github.com/vugu/vgrouter"
	"github.com/vugu/vugu"
	js "github.com/vugu/vugu/js"
)

type Root struct {
	vgrouter.NavigatorRef

	Greeting string
	ShowWasm bool `vugu:"data"`
	ShowGo   bool `vugu:"data"`
	ShowVugu bool `vugu:"data"`
}

func (r *Root) HandleDownloadFile(e vugu.DOMEvent) {
	var output bytes.Buffer

	// literal copy/paste from the encoding/csv godoc

	records := [][]string{
		{"first_name", "last_name", "username"},
		{"Rob", "Pike", "rob"},
		{"Ken", "Thompson", "ken"},
		{"Robert", "Griesemer", "gri"},
	}

	w := csv.NewWriter(&output)

	for _, record := range records {
		if err := w.Write(record); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}

	w.Flush()

	if err := w.Error(); err != nil {
		log.Fatal(err)
	}

	// important part

	dst := js.Global().Get("Uint8Array").New(len(output.Bytes()))
	_ = js.CopyBytesToJS(dst, output.Bytes())
	js.Global().Call("downloadFile", dst, "text/csv", "usernames.csv")
}
