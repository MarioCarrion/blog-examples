// +build wasm

package main

import (
	"flag"
	"fmt"

	"github.com/vugu/vgrouter"
	"github.com/vugu/vugu"
	"github.com/vugu/vugu/domrender"
)

func main() {
	mountPoint := flag.String("mount-point", "#vugu_mount_point", "The query selector for the mount point for the root component, if it is not a full HTML component")
	greeting := flag.String("greeting", "Hola", "Default greeting to use")

	flag.Parse()

	fmt.Printf("Entering main(), -mount-point=%q -greeting=%q\n", *mountPoint, *greeting)
	defer fmt.Printf("Exiting main()\n")

	renderer, err := domrender.New(*mountPoint)
	if err != nil {
		panic(err)
	}
	defer renderer.Release()

	buildEnv, err := vugu.NewBuildEnv(renderer.EventEnv())
	if err != nil {
		panic(err)
	}

	rootBuilder := vuguSetup(buildEnv,
		renderer.EventEnv(),
		*greeting,
	)

	for ok := true; ok; ok = renderer.EventWait() {
		buildResults := buildEnv.RunBuild(rootBuilder)

		if err := renderer.Render(buildResults); err != nil {
			panic(err)
		}
	}
}

func vuguSetup(buildEnv *vugu.BuildEnv, eventEnv vugu.EventEnv, greeting string) vugu.Builder {
	router := vgrouter.New(eventEnv)

	buildEnv.SetWireFunc(func(b vugu.Builder) {
		if c, ok := b.(vgrouter.NavigatorSetter); ok {
			c.NavigatorSet(router)
		}
	})

	root := &Root{
		Greeting: greeting,
	}

	buildEnv.WireComponent(root)

	if err := router.ListenForPopState(); err != nil {
		panic(err)
	}

	if err := router.Pull(); err != nil {
		panic(err)
	}

	return root
}
